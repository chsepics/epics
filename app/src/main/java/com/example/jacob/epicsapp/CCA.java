package com.example.jacob.epicsapp;

public class CCA extends MarkerTemplate {

    public CCA() {
        super(33.307316, -111.842541, "Chandler Center for the Arts", "Concert hall that doubles as an assembly venue.");
    }

}