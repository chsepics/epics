package com.example.jacob.epicsapp;

public class OldGym extends MarkerTemplate {

    public OldGym() {
        super(33.307970, -111.843147, "Old Gym","The Oldest of our Two Gyms.");
    }

}