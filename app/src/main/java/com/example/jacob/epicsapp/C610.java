package com.example.jacob.epicsapp;

public class C610 extends MarkerTemplate {
    public C610() {
        super(33.3093093d, -111.8429329d, "C610", "Teacher: Roger Murdock; Floor Two");
    }
}
