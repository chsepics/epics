package com.example.jacob.epicsapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_main);
    }

    public void button(View v) {
        EditText text = (EditText)
                super.findViewById(R.id.editText);
        String value = regularExpression(text.getText().toString());
        try {
            super.startActivity(new Intent(value));
        }
        catch (Exception e) {
            Toast.makeText(super.getApplicationContext(), "Invalid Entry. Try again.", Toast.LENGTH_SHORT).show();
        }
    }

    public void help (View v) {
        startActivity(new Intent("com.example.jacob.epicsapp.HELP"));
    }
    public String regularExpression(String tmpValue) {
        return tmpValue.toLowerCase().replace("#", "").replace("'", "").replace(".", "").replace("junior", "").replace("senior", "").replace("mrs", "").replace("jr", "").replace("sn", "").replace("mr","").replace("ms", "").replace("dr","").replaceAll("\\s+", "").replaceFirst("[a-z]", "");
    }

}