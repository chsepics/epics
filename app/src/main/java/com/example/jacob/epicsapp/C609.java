package com.example.jacob.epicsapp;

public class C609 extends MarkerTemplate {
    public C609() {
        super(33.3094878d, -111.8429329d, "C609, C611, C613", "Teachers: Walterson, Martin, Babb; Floor Two");
    }
}
